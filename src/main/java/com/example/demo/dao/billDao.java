package com.example.demo.dao;

import java.util.List;

import com.example.demo.model.bill;

public interface billDao {
	
    boolean insert(bill bil);
	
	List<bill> loadAllBill();
	
	bill findBillById(long bill_id);
	
	String findBillByCustomerId(long bill_id);
	
	int getTotalNumberBills();

	boolean modify(Integer id, bill bil);
	
	boolean delete(bill bil);
}
