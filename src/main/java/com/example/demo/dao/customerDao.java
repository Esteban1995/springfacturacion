package com.example.demo.dao;

import java.util.List;

import com.example.demo.model.customer;

public interface customerDao {
	
	boolean insert(customer cus);
	
	List<customer> loadAllCustomer();
	
	customer findCustomerById(long cust_id);
	
	String findNameById(long cust_id);
	
	int getTotalNumberCustomer();
	
	boolean modify(Integer id, customer cus);
	
	boolean delete(customer cus);

}
