package com.example.demo.dao;

import java.util.List;

import com.example.demo.model.detail;

public interface detailDao {

	boolean insert(detail det);

	List<detail> loadAlldetail();

	detail finddetailById(long det_id);

	String finddetailbybillId(long det_id);
	
	boolean modify(Integer id, detail det);
	
	boolean delete(detail det);
}
