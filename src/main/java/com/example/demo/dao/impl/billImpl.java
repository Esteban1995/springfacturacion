package com.example.demo.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.billDao;
import com.example.demo.model.bill;
import com.example.demo.model.customer;
@Repository
public class billImpl extends JdbcDaoSupport implements billDao {

    @Autowired 
    DataSource dataSource;
 
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
    @Override
    public boolean insert(bill bil) {
         String sql = "INSERT INTO bills " +
                 "(id_bill, cdate, id_customer, total) VALUES (?, ?, ?, ?)" ;
                     getJdbcTemplate().update(sql, new Object[]{
                     bil.getId_bill(), bil.getCdate(), bil.getId_customer(), bil.getTotal()
                    
                 });
                     return true;
    }

    @Override
    public List<bill> loadAllBill() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public bill findBillById(long bill_id) {
        String sql = "SELECT * FROM bills WHERE id_bill = ?";
        return (bill)getJdbcTemplate().queryForObject(sql, new Object[]{bill_id}, new RowMapper<bill>(){
            @Override
            public bill mapRow(ResultSet rs, int rwNumber) throws SQLException {
                bill bil = new bill();
                bil.setId_bill(rs.getInt("id_bill"));
                bil.setCdate(rs.getDate("cdate"));
                bil.setId_customer(rs.getInt("id_cliente"));
                bil.setTotal(rs.getDouble("total"));
                return bil;
            }
        });
    }

    @Override
    public String findBillByCustomerId(long bill_id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getTotalNumberBills() {
        // TODO Auto-generated method stub
        return 0;
    }
    @Override
    public boolean modify(Integer id, bill bil) {
        String sql = "UPDATE bills SET total = ? WHERE id_bill ="+id ;
         getJdbcTemplate().update(sql, new Object[]{
          bil.getTotal()
     });
        return true;
    }

    @Override
	public boolean delete(bill bil) {
		String sql = "DELETE FROM bills WHERE id_bill = ?";
		getJdbcTemplate().update(sql, new Object[] { 
				bil.getId_bill() });
		return true;
	}
}
