package com.example.demo.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.customerDao;
import com.example.demo.model.customer;
import com.jayway.jsonpath.ParseContext;

@Repository
public class customerImpl extends JdbcDaoSupport implements customerDao {

	@Autowired
	DataSource dataSource;
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(customer cus) {
		String sql = "INSERT INTO customers " + "(id_customer, name, lastname) VALUES (?, ?, ?)";
		getJdbcTemplate().update(sql, new Object[] { cus.getId_customer(), cus.getName(), cus.getLastname() });
		return true;
	}

	@Override
	public List<customer> loadAllCustomer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public customer findCustomerById(long id_customer) {
		String sql = "SELECT * FROM customers WHERE id_customer = ?";
		return (customer) getJdbcTemplate().queryForObject(sql, new Object[] { id_customer },
				new RowMapper<customer>() {
					@Override
					public customer mapRow(ResultSet rs, int rwNumber) throws SQLException {
						customer cust = new customer();
						cust.setId_customer(rs.getInt("id_customer"));
						cust.setName(rs.getString("name"));
						cust.setLastname(rs.getString("lastname"));
						return cust;
					}
				});
	}

	@Override
	public String findNameById(long cust_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getTotalNumberCustomer() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean modify(Integer id, customer cus) {
		String sql = "UPDATE customers SET name = ?,  lastname = ? WHERE id_customer =" + id;
		getJdbcTemplate().update(sql, new Object[] { 
				 cus.getName(), cus.getLastname()
				});
		return true;
	}

	@Override
	public boolean delete(customer cus) {
		String sql = "DELETE FROM customers WHERE id_customer = ?";
		getJdbcTemplate().update(sql, new Object[] { 
				cus.getId_customer() });
		return true;
	}

}
