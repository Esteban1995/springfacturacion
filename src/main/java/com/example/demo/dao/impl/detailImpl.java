package com.example.demo.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.detailDao;
import com.example.demo.model.detail;
@Repository
public class detailImpl extends JdbcDaoSupport implements detailDao {

	@Autowired 
    DataSource dataSource;
 
    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }
	
	@Override
	public boolean insert(detail det) {
		 String sql = "INSERT INTO details " +
				 "(id_detail, id_bill, id_vehicle, quantity, subtotal) VALUES (?, ?, ?, ?, ?)" ;
				     getJdbcTemplate().update(sql, new Object[]{
				     det.getId_detail(), det.getId_bill(), det.getId_vehicle(), det.getQuantity(), det.getSubtotal()
				 });
				     return true;

	}

	@Override
	public List<detail> loadAlldetail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public detail finddetailById(long det_id) {
		String sql = "SELECT * FROM details WHERE id_detail = ?";
		return (detail)getJdbcTemplate().queryForObject(sql, new Object[]{det_id}, new RowMapper<detail>(){
			@Override
			public detail mapRow(ResultSet rs, int rwNumber) throws SQLException {
				detail det_1 = new detail();
				det_1.setId_detail(rs.getInt("id_detail"));
				det_1.setId_bill(rs.getInt("id_bill"));
				det_1.setId_vehicle(rs.getInt("id_vehicle"));
				det_1.setQuantity(rs.getInt("quantity"));
				det_1.setSubtotal(rs.getDouble("subtotal"));
				return det_1;
			}
		});
	}

	@Override
	public String finddetailbybillId(long det_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean modify(Integer id, detail det) {
		String sql = "UPDATE details SET id_bill = ?,  id_vehicle = ?, quantity = ?, subtotal = ? WHERE id_detail =" + id;
		getJdbcTemplate().update(sql, new Object[] { 
				 det.getId_bill(), det.getId_vehicle(), det.getQuantity(), det.getSubtotal()
				});
		return true;
	}

	@Override
	public boolean delete(detail det) {
		String sql = "DELETE FROM details WHERE id_detail = ?";
		getJdbcTemplate().update(sql, new Object[] { 
				det.getId_detail() });
		return true;
	}

}
