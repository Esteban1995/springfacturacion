package com.example.demo.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.vehicleDao;
import com.example.demo.model.detail;
import com.example.demo.model.vehicle;
@Repository
public class vehicleDaoImpl extends JdbcDaoSupport implements vehicleDao {

	 @Autowired 
	    DataSource dataSource;
	    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	 
	    @PostConstruct
	    private void initialize(){
	        setDataSource(dataSource);
	    }
	@Override
	public void insert(vehicle veh) {
		 String sql = "INSERT INTO vehicles " +
				 "(id_vehicle, brand, model, price) VALUES (?, ?, ?, ?)" ;
				     getJdbcTemplate().update(sql, new Object[]{
				     veh.getId_vehicle(), veh.getBrand(), veh.getModel(), veh.getPrice()
				 });

	}

	@Override
	public List<vehicle> loadAllvehicles() {
		return null;
			
	}
//	@Override
//	public vehicle findVehicleById(long veh_id) {
//		String sql = "SELECT * FROM vehicles WHERE id_vehicle= ?";
//		return (vehicle)getJdbcTemplate().queryForObject(sql, new Object[]{veh_id}, new RowMapper<vehicle>(){
//			@Override
//			public vehicle mapRow(ResultSet rs, int rwNumber) throws SQLException {
//				vehicle veh_1 = new vehicle();
//				veh_1.setId_vehicle(rs.getInt("id_vehicle"));
//				veh_1.setBrand(rs.getString("brand"));
//				veh_1.setModel(rs.getString("model"));
//				veh_1.setPrice(rs.getDouble("price"));
//				return veh_1;
//			}
//		});
//	}
	@Override
	public vehicle findVehiclesById(long veh_id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public HashMap<Integer, String> findVehicles() {
		HashMap <Integer, String> marcaV= new HashMap<Integer, String>();
		marcaV.put(1, "Honda");
		marcaV.put(2, "Ferrari");
		marcaV.put(3, "Toyota");
		marcaV.put(4, "BMW");
		
		Set<Integer> keys = marcaV.keySet();
		for(Integer key:keys) {
			String valor = marcaV.get(key);
			System.out.println(valor);
		}
		
		return marcaV;
	}

}
