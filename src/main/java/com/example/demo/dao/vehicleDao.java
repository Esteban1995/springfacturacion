package com.example.demo.dao;

import java.util.HashMap;
import java.util.List;

import com.example.demo.model.vehicle;

public interface vehicleDao {

    void insert(vehicle veh);
	
	List<vehicle> loadAllvehicles();
	
	vehicle findVehiclesById(long veh_id);
	
	HashMap<Integer, String> findVehicles();
	
}
