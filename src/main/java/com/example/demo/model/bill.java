package com.example.demo.model;

import java.io.Serializable;
import java.util.Date;

public class bill implements Serializable{

	private static final long serialVersionUID = 2L;
	
	Integer id_bill;
	Date cdate;
	Integer id_customer;
	Double total;
	
	public bill() {
		
	}
	
	public bill(Integer id_bill, Date cdate, Integer id_customer, Double total) {
		this.id_bill=id_bill;
		this.cdate=cdate;
		this.id_customer=id_customer;
		this.total=total;
	}

	public Integer getId_bill() {
		return id_bill;
	}

	public void setId_bill(Integer id_bill) {
		this.id_bill = id_bill;
	}

	public Date getCdate() {
		return cdate;
	}

	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}

	public Integer getId_customer() {
		return id_customer;
	}

	public void setId_customer(Integer id_customer) {
		this.id_customer = id_customer;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "bill [id_bill=" + id_bill + ", creation_date=" + cdate + ", id_customer=" + id_customer
				+ ", total=" + total + "]";
	}

}
