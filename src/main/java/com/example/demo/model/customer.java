package com.example.demo.model;

import java.io.Serializable;

public class customer implements Serializable{

	private static final long serialVersionUID = 1L;
	
	Integer id_customer;
	String name;
	String lastname;
	
	public customer() {
		
	}
	
	public customer(Integer id_customer, String name, String lastname) {
		this.id_customer=id_customer;
		this.name=name;
		this.lastname=lastname;
	}

	public Integer getId_customer() {
		return id_customer;
	}

	public void setId_customer(Integer id_customer) {
		this.id_customer = id_customer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "customer [id_customer=" + id_customer + ", name=" + name + ", lastname=" + lastname + "]";
	}
	
}
