package com.example.demo.model;

import java.io.Serializable;

public class detail implements Serializable{

	
	private static final long serialVersionUID = 3L;
	
	Integer id_detail;
	Integer id_bill;
	Integer id_vehicle;
	Integer quantity;
	Double subtotal;
	
	public detail() {
		
	}
	
	public detail(Integer id_detail, Integer id_bill, Integer id_vehicle, Integer quantity, Double subtotal) {
		this.id_detail=id_detail;
		this.id_bill=id_bill;
		this.id_vehicle=id_vehicle;
		this.quantity=quantity;
		this.subtotal=subtotal;
	}

	public Integer getId_detail() {
		return id_detail;
	}

	public void setId_detail(Integer id_detail) {
		this.id_detail = id_detail;
	}

	public Integer getId_bill() {
		return id_bill;
	}

	public void setId_bill(Integer id_bill) {
		this.id_bill = id_bill;
	}

	public Integer getId_vehicle() {
		return id_vehicle;
	}

	public void setId_vehicle(Integer id_vehicle) {
		this.id_vehicle = id_vehicle;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	@Override
	public String toString() {
		return "detail [id_detail=" + id_detail + ", id_bill=" + id_bill + ", id_vehicle=" + id_vehicle + ", quantity="
				+ quantity + ", subtotal=" + subtotal + "]";
	}
	
}
