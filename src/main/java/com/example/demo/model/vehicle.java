package com.example.demo.model;

import java.io.Serializable;

public class vehicle implements Serializable{

	private static final long serialVersionUID = 4L;
	
	Integer id_vehicle;
	String brand;
	String model;
	Double price;
	
	public vehicle() {
		
	}
	
	public vehicle(Integer id_vehicle, String brand, String model , Double price) {
		this.id_vehicle=id_vehicle;
		this.brand=brand;
		this.model=model;
		this.price=price;
	}

	public Integer getId_vehicle() {
		return id_vehicle;
	}

	public void setId_vehicle(Integer id_vehicle) {
		this.id_vehicle = id_vehicle;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "vehicle [id_vehicle=" + id_vehicle + ", brand=" + brand + ", model=" + model + ", price=" + price + "]";
	}

	

}
