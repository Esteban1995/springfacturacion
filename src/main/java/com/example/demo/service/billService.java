package com.example.demo.service;

import java.util.List;

import com.example.demo.model.bill;

public interface billService {

	boolean insert(bill bil);
	
	void insertBatch(List<bill> bills);
	
	void loadAllbill();
	
	void getBillById(long bill_id);
	
	void getTotalNumberBill();
	
	boolean modify(Integer id, bill bil);
	
	boolean delete(bill bil);
}
