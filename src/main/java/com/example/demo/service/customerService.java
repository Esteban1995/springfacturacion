package com.example.demo.service;

import java.util.List;

import com.example.demo.model.customer;

public interface customerService {
	boolean insert(customer cus);
	
	void insertBatch(List<customer> customers);
	
	void loadAllCustomer();
	
	void getCustomerById(long cust_id);
	
	void getCustomerNameById(long cust_id);
	
	void getTotalNumerCustomer();
	
	boolean modify(Integer id, customer cus);
	
	boolean delete(customer cus);
}
