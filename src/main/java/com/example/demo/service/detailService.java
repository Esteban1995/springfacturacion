package com.example.demo.service;

import java.util.List;

import com.example.demo.model.detail;

public interface detailService {

	boolean insert(detail det);
	
	void insertBatch(List<detail> details);
	
	void loadAlldetails();
	
	void getDetailById(long det_id);
	
	void getTotalNumerDetail();
	
	boolean modify(Integer id, detail det);
	
	boolean delete(detail det);
}
