package com.example.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.billDao;
import com.example.demo.model.bill;
import com.example.demo.service.billService;
@Service
public class billServiceImpl implements billService {

	@Autowired
	billDao billDao;
	
	@Override
	public boolean insert(bill bil) {
		billDao.insert(bil);
		return true;
	}

	@Override
	public void insertBatch(List<bill> bills) {
		// TODO Auto-generated method stub

	}

	@Override
	public void loadAllbill() {
		// TODO Auto-generated method stub

	}

	@Override
	public void getBillById(long bill_id) {
		bill bil = billDao.findBillById(bill_id);
		System.out.println(bil);

	}

	@Override
	public void getTotalNumberBill() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean modify(Integer id, bill bil) {
		billDao.modify(id, bil);
		return true;
	}

	@Override
	public boolean delete(bill bil) {
		billDao.delete(bil);
		return true;
	}

}
