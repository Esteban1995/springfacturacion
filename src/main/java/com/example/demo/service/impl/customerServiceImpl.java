package com.example.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.customerDao;
import com.example.demo.model.customer;

@Service
public class customerServiceImpl implements com.example.demo.service.customerService {

	@Autowired 
	 customerDao customerDao;
	
	@Override
	public boolean insert(customer cus) {
		customerDao.insert(cus);
		return true;
	}

	@Override
	public void insertBatch(List<customer> customers) {
		

	}

	@Override
	public void loadAllCustomer() {
		// TODO Auto-generated method stub

	}

	@Override
	public void getCustomerById(long id_customer) {
		customer cust = customerDao.findCustomerById(id_customer);
		System.out.println(cust);

	}

	@Override
	public void getCustomerNameById(long cust_id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getTotalNumerCustomer() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean modify(Integer id, customer cus) {
		customerDao.modify(id, cus);
		return true;
	}

	@Override
	public boolean delete(customer cus) {
		customerDao.delete(cus);
		return true;
	}

	

}
