package com.example.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.demo.model.detail;
import com.example.demo.service.detailService;
import com.example.demo.dao.detailDao;

@Service
public class detailServiceImpl implements detailService {

	@Autowired
	detailDao detailDao;

	@Override
	public boolean insert(detail det) {
		detailDao.insert(det);
		return true;
	}

	@Override
	public void insertBatch(List<detail> details) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadAlldetails() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getDetailById(long det_id) {
		detail det = detailDao.finddetailById(det_id);
		System.out.println(det);
		
	}

	@Override
	public void getTotalNumerDetail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean modify(Integer id, detail det) {
		detailDao.modify(id, det);
		return true;
	}

	@Override
	public boolean delete(detail det) {
		detailDao.delete(det);
		return true;
	}


}
