package com.example.demo.service;


import java.util.HashMap;

import com.example.demo.model.vehicle;

public interface vehicleService {

	void insert(vehicle veh);
	
	void loadAllCustomer();
	
	void getvehicleById(long veh_id);
	
	void getVehicleNameById(long veh_id);
	
	void getTotalNumerCustomer();
	
	void modify(Integer id, vehicle veh);
	HashMap<Integer, String> findVehicles();
	
}
