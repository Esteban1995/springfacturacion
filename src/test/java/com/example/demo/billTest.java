package com.example.demo;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.bill;
import com.example.demo.service.billService;
@RunWith(SpringRunner.class)
@SpringBootTest
public class billTest {

	@Autowired
	billService bilService;

	@Test
	public void TestInsertBill() {
		 Date da = new Date();
		 bill bill_1 = new bill();
         bill_1.setId_bill(556154);
         bill_1.setCdate(da);
         bill_1.setId_customer(1872837);
         bill_1.setTotal(582.00);
		
		assertEquals(true, bilService.insert(bill_1));
	}
	
	@Test
	public void TestModify() {
		bill bil = new bill();
		bil.setTotal(250.00);
		
		assertEquals(true, bilService.modify(689, bil));
	}
	
	@Test
	public void TestDelete() {
		bill bil = new bill();
		bil.setId_bill(55461);
		
		assertEquals(true, bilService.delete(bil));
	}

}
