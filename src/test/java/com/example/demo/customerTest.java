package com.example.demo;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.customer;
import com.example.demo.service.customerService;
@RunWith(SpringRunner.class)
@SpringBootTest
public class customerTest {
	@Autowired
	 customerService cusService;
	@Test
	public void TestInsert() {
		customer cus = new customer();
		cus.setId_customer(100);
		cus.setName("gol");
		cus.setLastname("anulado");
		
		assertEquals(true, cusService.insert(cus));
	}
	@Test
	public void TestModify() {
		customer cus = new customer();
		cus.setName("Gerardo");
		cus.setLastname("Hernandez");
		
		assertEquals(true, cusService.modify(148430, cus));
	}
	
	@Test
	public void TestDelete() {
		customer cus = new customer();
		cus.setId_customer(148430);
		
		assertEquals(true, cusService.delete(cus));
	}
	

}
