package com.example.demo;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.detail;
import com.example.demo.service.detailService;
@RunWith(SpringRunner.class)
@SpringBootTest
public class detailTest {

	@Autowired
	detailService detService;

	@Test
	public void TestInsertBill() {
		 detail det = new detail();
         det.setId_detail(12);
         det.setId_bill(689);
         det.setId_vehicle(15);
         det.setQuantity(1);
         det.setSubtotal(582.00);
		
		assertEquals(true, detService.insert(det));
	}
	
	@Test
	public void TestModify() {
		detail det = new detail();
		det.setId_bill(4);
		det.setId_vehicle(6);
		det.setQuantity(1);
		det.setSubtotal(250.00);
		
		assertEquals(true, detService.modify(12, det));
	}
	
	@Test
	public void TestDelete() {
		detail det = new detail();
		det.setId_detail(5245);
		
		assertEquals(true, detService.delete(det));
	}

}
